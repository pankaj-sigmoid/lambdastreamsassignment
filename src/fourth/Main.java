package fourth;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Student> studentList=new ArrayList<>();
        Scanner sc=new Scanner(System.in);

        System.out.println("number of students= ");
        int n=sc.nextInt();

        System.out.println("Students name and mark: ");
        for(int i=0;i<n;i++)
            studentList.add(new Student(sc.next(), sc.nextInt()));

        Map<Boolean,List<Student>> result=studentList.stream().collect(Collectors.partitioningBy(s -> s.getMark()>=40));


        Map<String,List<Student>> ans=new HashMap<>();
        ans.put("PASS",result.get(true));
        ans.put("FAIL",result.get(false));

        System.out.println("students passed: ");
        for(Student student:ans.get("PASS")){
            System.out.println(student.getName());
        }

        System.out.println("students failed: ");
        for(Student student:ans.get("FAIL")){
            System.out.println(student.getName());
        }
    }
}
