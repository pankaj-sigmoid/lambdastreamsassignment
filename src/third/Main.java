package third;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Department de=new Department("DE",1,"Mayur");
        Department ds=new Department("DS",2,"Rahul");
        Department dev=new Department("DEVOPS",3,"Nitin");

        List<Employee> employeeList=new ArrayList<>();
        employeeList.add(new Employee("Pankaj","Kumar",de,20000));
        employeeList.add(new Employee("Mayur","Kumar",de,30000));
        employeeList.add(new Employee("Abhinav","Kumar",de,40000));
        employeeList.add(new Employee("Rahul","Kumar",ds,50000));
        employeeList.add(new Employee("Pulak","Kumar",ds,60000));
        employeeList.add(new Employee("Abhi","Kumar",ds,70000));
        employeeList.add(new Employee("Sanket","Kumar",dev,80000));
        employeeList.add(new Employee("Rishabh","Kumar",dev,90000));

        Map<Department,Integer> result=employeeList.stream()
                                                   .collect(Collectors.groupingBy(Employee::getDepartment,
                                                                                  Collectors.summingInt(Employee::getSalary)));

        for(Department d:result.keySet()){
            System.out.println(d.getName()+" "+result.get(d));
        }

    }
}
