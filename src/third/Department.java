package third;

public class Department {
    private String name;
    private int deptId;
    private String deptHead;

    public Department(String name, int deptId, String deptHead) {
        this.name = name;
        this.deptId = deptId;
        this.deptHead = deptHead;
    }

    public String getName() {
        return name;
    }

    public int getDeptId() {
        return deptId;
    }

    public String getDeptHead() {
        return deptHead;
    }
}
