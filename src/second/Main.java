package second;

public class Main {
    public static void main(String[] args) {
        CustomThreadPool pool=new CustomThreadPool(3);
        for(int i=0;i<10;i++){
            TaskForAThread task=new TaskForAThread(i);
            pool.execute(task);
        }
        pool.markShutdown();
    }
}
