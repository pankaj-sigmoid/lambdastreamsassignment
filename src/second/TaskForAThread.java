package second;

public class TaskForAThread implements Runnable {
    private int n;

    public TaskForAThread(int n) {
        this.n = n;
    }

    public void run(){
        System.out.println("thread "+n+" is started");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("thread "+n+" is ended");
    }
}
