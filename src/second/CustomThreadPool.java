package second;


import java.util.concurrent.LinkedBlockingQueue;

public class CustomThreadPool {
    private final int nThreads;
    private final Worker[] threads;
    private final LinkedBlockingQueue<Runnable> queue;
    private boolean initiateShutdown;

    public CustomThreadPool(int nThreads){
        this.nThreads=nThreads;
        threads=new Worker[nThreads];
        queue=new LinkedBlockingQueue();
        initiateShutdown=false;

        for(int i=0;i<nThreads;i++){
            threads[i]=new Worker();
            threads[i].start();
        }
    }

    public void markShutdown(){
        initiateShutdown=true;
    }

    public void execute(Runnable task){
        synchronized (queue){
            queue.add(task);
            queue.notify();
        }
    }

    private class Worker extends Thread{
        public void run(){
            try {
                while (!initiateShutdown || !queue.isEmpty()) {
                    Runnable task;
                    while ((task = queue.poll()) != null) {
                        task.run();
                    }
                    Thread.sleep(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
