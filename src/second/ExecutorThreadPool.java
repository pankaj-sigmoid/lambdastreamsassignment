package second;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorThreadPool {
    static final int MaxThreadPoolSize=3;

    public static void main(String[] args) {
        Runnable t1=new TaskForAThread(1);
        Runnable t2=new TaskForAThread(2);
        Runnable t3=new TaskForAThread(3);
        Runnable t4=new TaskForAThread(4);
        Runnable t5=new TaskForAThread(5);

        ExecutorService pool= Executors.newFixedThreadPool(MaxThreadPoolSize);

        pool.execute(t1);
        pool.execute(t2);
        pool.execute(t3);
        pool.execute(t4);
        pool.execute(t5);

        pool.shutdown();
    }
}
