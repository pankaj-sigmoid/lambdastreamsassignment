package first;

public class Main {

    public static String betterString(String a, String b, TwoStringPredicate tsp) {
        if(tsp.findBetterString(a,b)) return a;
        else return b;
    }
    public static void main(String[] args) {
        String a="abc";
        String b="abdc";

        String res=betterString(a,b,(s1,s2)->true);
        String res1=betterString(a,b,(s1,s2)-> false);
        System.out.println(res);        //return first string if lambda return true
        System.out.println(res1);       //return second string if lambda return false

    }
}
