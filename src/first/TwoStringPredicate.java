package first;

public interface TwoStringPredicate {
    public boolean findBetterString(String a, String b);
}
